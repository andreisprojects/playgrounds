//: Playground - noun: a place where people can play

import UIKit

//breaking out of the loop

var count = 10

while count >= 0 {
    print(count)
    
    if count == 5 {
        print("Let's stsrt now!")
        break
    }
    count -= 1
}

print("Start!")

//exiting multiple loops

outer: for x in  1...10 {
    for y in  1...10 {
        let product = x * y
        print("\(x) * \(y) = \(product)")
        
        if product == 50 {
            print("It's a bullseye!")
            break outer
        }
    }
}

//skipping items

for i in 1...10 {
    if i % 2 == 1 {
//        print("odd")
        continue
    }
    print("even number: ", i)
}

//infinite loops

var counter = 0

while true {
    counter += 1
    
    if counter == 10 {
        print("Done!")
        break
    }
}
