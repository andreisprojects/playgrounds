
import UIKit

//Enums + associated values
enum Language {
    case objc
    case swift(version: Float)
}

var myFavoriteLanguage = Language.swift(version: 4.1)

// Enum raw values (specific type needed)
//automatically starts counting from 0... assign 1 to the first case to start from 1
enum SwiftVersion: Int {
    case swift1 = 1
    case swift2
    case swift3
    case swift4
}

let latestSwiftVersion = SwiftVersion(rawValue: 4)
if let lv =  latestSwiftVersion {
    print(lv)
}

// or assign those raw values right off the bat
enum Currencies: String {
    case dollar = "USD"
    case euro =  "EUR"
    case pound = "GBP"
}

let currencyTickerValue = Currencies.dollar.rawValue
print(currencyTickerValue)

//Switch statement

let currencyTicker = Currencies.dollar

switch currencyTicker {
case Currencies.dollar:
    print("The U.S. Dollar")
case Currencies.euro:
    print("The Euro")
case Currencies.pound:
    print("The British Pound")
}

//if you use enum, you don't need a default value, otherwise you do
let currency = "GBP"

switch currency {
case "USD":
    print("The U.S. Dollar")
case "EUR":
    print("The Euro")
case "GBP":
    print("The British Pound")
default:
    print("Some other currency")
}

//use fallthrough to continue 

