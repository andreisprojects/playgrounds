//: Playground - noun: a place where people can play

import UIKit

//default parameters
func manage(_ asset: String, hold: Bool = true) {
    if hold == true {
        print("buy and hold \(asset)")
    } else {
        print("sell \(asset)")
    }
}

manage("gold")
manage("bitcoin", hold: false)

//variadic parameters

func sum(numbers: Int...) {
    let sum = numbers.reduce(0,+)
    print("the sum is", sum)
}

sum(numbers: 1,2,3,4,5)

//throwing functions

enum PassError: Error {
    case short
}

func checkPassword(password: String) throws -> Bool {
    if password.count <= 5 {
        throw PassError.short
    }
    return true
}

//running throwing functions

do {
    try checkPassword(password: "qwerty")
    print("access granted")
} catch {
    print("the password is too short")
}

//inout parameters (parameters that can be changed inside the function - changing the value of the variable used as the parameter rathter than returning a new value)

func squareInPlace(number: inout Int) {
    number *= number
}

var number = 5

squareInPlace(number: &number)
print(number)

//instead of inout we could return a new value and stor it in let

func square(number: Int) -> Int {
    return number * number
}

let squaredNumber = square(number: 5)
print(squaredNumber)
